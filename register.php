<?php
include_once 'database/koneksi.php';
require "vendor/autoload.php";
use \Firebase\JWT\JWT;

header("Access-Control-Allow-Origin: * ");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$name       = '';
$username   = '';
$email      = '';
$password   = '';

$phone      = '';
$country    = '';
$city       = '';
$address    = '';
$postcode   = '';
$conn       = null;

$databaseService = new DatabaseService();
$conn = $databaseService->getConnection();

$data = json_decode(file_get_contents("php://input"));

$name       = $data->name;
$username   = $data->username;
$email      = $data->email;
$password   = $data->password;

$phone      = $data->phone;    
$country    = $data->country;
$city       = $data->city;
$address    = $data->address;
$postcode   = $data->postcode;

$table_name = 'user';

$query = "INSERT INTO " . $table_name . "
                SET name = :name,
                    username = :username,
                    email = :email,
                    phone = :phone,
                    country = :country,
                    city = :city,
                    address = :address,
                    postcode = :postcode,
                    password = :password";

$stmt = $conn->prepare($query);

$stmt->bindParam(':name', $name);
$stmt->bindParam(':username', $username);
$stmt->bindParam(':email', $email);
$stmt->bindParam(':phone', $phone);
$stmt->bindParam(':country', $country);
$stmt->bindParam(':city', $city);
$stmt->bindParam(':address', $address);
$stmt->bindParam(':postcode', $postcode);

$password_hash = password_hash($password, PASSWORD_BCRYPT);

$stmt->bindParam(':password', $password_hash);


if($stmt->execute()){
    http_response_code(200);

    // 15 * 60 (detik) = 15 menit
    // $expired_time = time() + (15 * 60);

    // $payload = [
    //     'email' => $data->email,
    //     'exp'   => $expired_time
    // ];
    // $token = JWT::encode($payload, $_ENV['ACCESS_TOKEN_SECRET']);
    
    echo json_encode(
        array(
            "message"   => "User was successfully registered.",
            // "token"     => $token,
            "email"     => $email,
            "username"  => $username
        ));
}
else{
    http_response_code(400);
    echo json_encode(array("message" => "Unable to register the user."));
}
?>