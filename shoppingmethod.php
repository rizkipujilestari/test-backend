<?php
require_once "database/koneksi.php";

class Shopping 
{
   public function get_all()
   {
      global $mysqli;
      $query    = "select * FROM shopping";
      $data     = array();
      $result   = $mysqli->query($query);

      if ($result->num_rows > 0) 
      { 
         // return true; 
         while ($row=mysqli_fetch_object($result))
         {
            $data[]=$row;
         }
      } else { 
         // return false; 
         $data="";
      }

      $response = array(
                  'data' => $data
                  );
      header('Content-Type: application/json');
      echo json_encode($response);
   }
 
    public function getby_id($id=0)
    {
      global $mysqli;
      $query   = "SELECT * FROM shopping";
      if ($id != 0)
      {
         $query.=" WHERE id=".$id." LIMIT 1";
      }
      $data    = array();
      $result  = $mysqli->query($query);
      
      if ($result->num_rows > 0) 
      { 
         // return true; 
         while ($row=mysqli_fetch_object($result))
         {
            $data[]=$row;
         }
      } else { 
         // return false; 
         $data="";
      }
      
      $response=array(
                     'data' => $data
                  );
      header('Content-Type: application/json');
      echo json_encode($response);
        
   }
 
   public function create_new()
   {
      global $mysqli;
      $arrcheckpost = array('id' => '', 'name' => '', 'createddate' => '');
      $hitung = count(array_intersect_key($_POST, $arrcheckpost));
      if($hitung == count($arrcheckpost)) {
         
            $result = mysqli_query($mysqli, "INSERT INTO `shopping` SET
            name = '$_POST[name]',
            createddate = '$_POST[jk]',
            ");
               
            if($result)
            {
               $response=array(
                  'status' => 1,
                  'message' =>'New Shopping Added Successfully.',
                  'data' => json_encode($arrcheckpost)
               );
            }
            else
            {
               $response=array(
                  'status' => 0,
                  'message' =>'New Shopping Addition Failed.'
               );
            }
      }else{
         $response=array(
                  'status' => 0,
                  'message' =>'Parameter Do Not Match'
               );
      }
      header('Content-Type: application/json');
      echo json_encode($response);
   }

}
 
?>