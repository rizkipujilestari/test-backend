<?php
require_once "shoppingmethod.php";

$shop = new Shopping();
$request_method=$_SERVER["REQUEST_METHOD"];
switch ($request_method) {
    case 'GET':
        if(!empty($_GET["id"]))
        {
            $id=intval($_GET["id"]);
            $shop->getby_id($id);
        }
        else
        {
            $shop->get_all();
        }
        break;
    case 'POST':
        if(empty($_GET["id"]))
        {
        $shop->create_new();
        } 
        break;         
    default:
        // Invalid Request Method
        header("HTTP/1.0 405 Method Not Allowed");
        break;
    break;
}
